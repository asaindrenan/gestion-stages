var include = /^vendor|^bower_components/;
var exclude = /^bower_components\/handlebars/;

exports.config = {
	path: {
		app: "app"
	},
	files: {
		javascripts: {
			defaultExtension: "js",
			joinTo: {
				"js/app.min.js": /^app/,
				"js/lib.min.js": function (path) {
					return !exclude.test(path) && include.test(path);
				}
			},
			order: {
				before: [
					"bower_components/jquery/dist/jquery.js",
					"bower_components/underscore/underscore.js",
					"bower_components/backbone/backbone.js",
					"vendor/scripts/backbone.babysitter.js",
					"vendor/scripts/backbone.marionette.js",
					"vendor/scripts/backbone.radio.js",
					"vendor/scripts/moment.js",
				]
			}
		},
		stylesheets: {
			defaultExtension: "css",
			joinTo: "css/app.min.css",
			order: {
				before: [
					'bower_components/font-awesome/css/font-awesome.css'
				]
			}
		},
		templates: {
			defaultExtension: "hbs",
			joinTo: "js/app.min.js"
		}
	},
	overrides: {
    DEV: {
        optimize: true,
        sourceMaps: true
    }
  },
	plugins: {
		browserSync: {
			port: 3333,
			logLevel: "debug"
		}
	},
	server: {
		port: 3333,
		run: true
	}
};