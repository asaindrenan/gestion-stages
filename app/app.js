var Marionette = require('marionette');
var _ = require('underscore');

module.exports = Marionette.Application.extend({
	initialize: function() {
		this._subApps = {};
	},

	addSubApp: function(name, options) {
		var subAppOptions = _.omit(options, 'subAppClass');
		var subApp = new options.subAppClass(subAppOptions);
		this._subApps[name] = subApp;
	}
});