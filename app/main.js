require('./bootstrap');

var App = require('./app');
var radio = require('./radio');
var app = new App();

window.api = 'http://188.166.65.247/root/api/';

var Login = require('./apps/login');
var Header = require('./apps/header');
var Sidebar = require('./apps/sidebar');
var Dashboard = require('./apps/dashboard');
var Users = require('./apps/users');
var Stages = require('./apps/stages');
var Entreprises = require('./apps/entreprises');
var Classes = require('./apps/classes');
var Anneescolaires = require('./apps/anneescolaires');
var Filieres = require('./apps/filieres');
var Specialites = require('./apps/specialites'); 

var loginChannel = radio.channel('login');

app.addRegions({
  mainRegion: '#gestage-main',
  headerRegion: '#gestage-header',
  sidebarRegion: '#gestage-sidebar',
  loginRegion: '#gestage-login'
});

app.addSubApp('login', {
    subAppClass: Login,
    container: app.loginRegion
});

app.addSubApp('header', {
    subAppClass: Header,
    container: app.headerRegion
});

app.addSubApp('sidebar', {
    subAppClass: Sidebar,
    container: app.sidebarRegion
});

app.addSubApp('dashboard', {
    subAppClass: Dashboard,
    container: app.mainRegion
});

app.addSubApp('users', {
    subAppClass: Users,
    container: app.mainRegion
});

app.addSubApp('stages', {
    subAppClass: Stages,
    container: app.mainRegion
});

app.addSubApp('entreprises', {
    subAppClass: Entreprises,
    container: app.mainRegion
});

app.addSubApp('classes', {
    subAppClass: Classes,
    container: app.mainRegion
});

app.addSubApp('anneescolaires', {
    subAppClass: Anneescolaires,
    container: app.mainRegion
});

app.addSubApp('filieres', {
    subAppClass: Filieres,
    container: app.mainRegion
});

app.addSubApp('specialites', {
    subAppClass: Specialites,
    container: app.mainRegion
});

loginChannel.comply('set:user', function(data) {
  app.options.user = data;
});

loginChannel.comply('auth:success', function(user) {
  app.options.user = user;
  Handlebars.registerHelper('ifRole', function() {
    var options = arguments[arguments.length-1];
    var fnTrue=options.fn, fnFalse=options.inverse;
    var roles = _.map(arguments, function(value, index) {
      return value;
    });
    roles.pop();
    return ($.inArray(parseInt(app.options.user.IDROLE), roles) > -1) ? fnTrue() : fnFalse();
  });
  $('#gestage-login').hide();
  app._subApps.header.showHeader();
  app._subApps.sidebar.showSidebar();
  app._subApps.dashboard.showDashboard();
});

var originalSync = Backbone.sync;
Backbone.sync = function(method, model, options){
    if(!app.options.user){
      Backbone.history.navigate('', true);
    } else {
      options.beforeSend = function(xhr){
          var credentials = btoa(app.options.user.LOGINUTILISATEUR+':'+app.options.user.MDPUTILISATEUR);
          xhr.setRequestHeader('Authorization', credentials);
      };
      var deferred = $.Deferred();
      deferred.then(options.success, options.error);
      var response = originalSync(method, model, _.omit(options, 'success', 'error'));
      response.done(deferred.resolve);
      response.fail(function(){
        if(response.status == 401){
          alert('Échec de connexion.');
          Backbone.history.navigate('', true);
        }
      });
      return deferred.promise();
    }
};

Backbone.history.start();

if(!app.user) {
  app._subApps.login.router.navigate('/login');
  app._subApps.login.showLogin();
}