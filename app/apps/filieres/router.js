var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'filieres': 'showFilieres',
		'filiere/edit/:id': 'editFiliere',
		'filieres/new': 'createFiliere'
	}
});