var Marionette = require('marionette');
var Backbone = require('backbone');
var radio = require('../../radio');

var Router = require('./router');

var ListView = require('./views/list');
var SearchView = require('./views/search');
var ListLayoutView = require('./views/listLayout');
var EditView = require('./views/edit');
var NewView  = require('./views/new');

var Filiere  = require('./models/filiere');
var Filieres = require('./models/filieres');

var filiereChannel = radio.channel('filiere');

module.exports = Marionette.Object.extend({
	initialize: function(options) {
		this.container = options.container;
		this.router = new Router({ controller: this });
	},

  showFilieres: function() {
    var that = this;
    var filieres = new Filieres();
    filieres.fetch({
      success: function(filieres){
        that.view = new ListLayoutView();
        that.container.show(that.view);
        that.view.search.show(new SearchView());
        that.view.list.show(new ListView({ collection: filieres }));
      }
    });
    filiereChannel.on('delete:filiere', function(id) {
      var filiere = new Filiere({ NUMFILIERE: id });
      filiere.destroy();
    });
  },

  editFiliere: function(id) {
    var that = this;
    var filiere = new Filiere({ NUMFILIERE: id });
    filiere.fetch({
      success: function(filiere) {
        that.view = new EditView({ model: filiere });
        that.view.on('edit:filiere', function(data) {
          that.view.model.save( data, {
            success: function() {
              that.showFilieres();
              that.router.navigate('/filieres');
            }
          })
        });
        that.container.show(that.view);
      }
    });
  },

  createFiliere: function() {
    var that = this;
    var filiere = new Filiere();
    this.view = new NewView({ model: filiere });
    this.view.on('save:filiere', function(data) {
      filiere.save( data, {
        success: function(filiere) {
          that.showFilieres();
          that.router.navigate('/filieres');
        }
      });
    });
    this.container.show(this.view);
  }
});