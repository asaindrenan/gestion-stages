var Marionette = require('marionette');
var radio = require('../../../radio');

var FiliereView = require('./filiere');
var EmptyView = require('./empty');

var filiereChannel = radio.channel('filiere');

module.exports = Marionette.CompositeView.extend({
	template: require('../templates/list'),
	childView: FiliereView,
	emptyView: EmptyView,
	childViewContainer: 'tbody',

	onShow: function() {
		var that = this;
		filiereChannel.on('delete:filiere', function(id) {
			that.collection.remove({ id: id });
		});
	}
});