var Marionette = require('marionette');
var radio      = require('../../../radio');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/edit'),
	className: 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main',
	
	events: {
		'click .btn': 'edit'
	},

	edit: function(e) {
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this);
		this.trigger('edit:filiere', data);
	}
});