var Marionette = require('marionette');
var radio = require('../../../radio');

var filiereChannel = radio.channel('filiere');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/filiere'),
	tagName: 'tr',

	ui: {
		'delete': '.delete'
	},

	events: {
		'click .delete': 'delete'
	},

	delete: function() {
		if(confirm('Êtes-vous sure de vouloir supprimer cette année scolaire?')) {
			filiereChannel.trigger('delete:filiere', this.ui.delete.data('id'));
		}
	}
});