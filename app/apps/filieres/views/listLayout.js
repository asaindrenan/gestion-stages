var Marionette = require('marionette');

module.exports = Marionette.LayoutView.extend({
	template: require('../templates/listLayout'),
	regions: {
		search: '#filiere-search',
		list: '#filieres-list'
	},
	className: 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main'
});