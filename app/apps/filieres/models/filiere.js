var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
	urlRoot: api + 'filieres',

	idAttribute: 'NUMFILIERE',

	defaults: {
		'LIBELLEFILIERE': ''
	}
});