var Marionette = require('marionette');
var radio = require('../../../radio');

var AnneescolaireView = require('./anneescolaire');
var EmptyView = require('./empty');

var anneescolaireChannel = radio.channel('anneescolaire');

module.exports = Marionette.CompositeView.extend({
	template: require('../templates/list'),
	childView: AnneescolaireView,
	emptyView: EmptyView,
	childViewContainer: 'tbody',

	onShow: function() {
		var that = this;
		anneescolaireChannel.on('delete:anneescolaire', function(id) {
			that.collection.remove({ id: id });
		});
	}
});