var Marionette = require('marionette');
var radio      = require('../../../radio');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/search'),
});