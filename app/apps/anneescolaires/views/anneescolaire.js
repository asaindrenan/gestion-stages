var Marionette = require('marionette');
var radio = require('../../../radio');

var anneescolaireChannel = radio.channel('anneescolaire');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/anneescolaire'),
	tagName: 'tr',

	ui: {
		'delete': '.delete'
	},

	events: {
		'click .delete': 'delete'
	},

	delete: function() {
		if(confirm('Êtes-vous sure de vouloir supprimer cette année scolaire?')) {
			anneescolaireChannel.trigger('delete:anneescolaire', this.ui.delete.data('id'));
		}
	}
});