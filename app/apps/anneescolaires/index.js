var Marionette = require('marionette');
var Backbone = require('backbone');
var radio = require('../../radio');

var Router = require('./router');

var ListView = require('./views/list');
var SearchView = require('./views/search');
var ListLayoutView = require('./views/listLayout');
var ShowView = require('./views/show');
var EditView = require('./views/edit');
var NewView  = require('./views/new');

var Anneescolaire  = require('./models/anneescolaire');
var Anneescolaires = require('./models/anneescolaires');

var anneescolaireChannel = radio.channel('anneescolaire');

module.exports = Marionette.Object.extend({
	initialize: function(options) {
		this.container = options.container;
		this.router = new Router({ controller: this });
	},

  showAnneescolaires: function() {
    var that = this;
    var anneescolaires = new Anneescolaires();
    anneescolaires.fetch({
      success: function(anneescolaires){
        that.view = new ListLayoutView();
        that.container.show(that.view);
        that.view.search.show(new SearchView());
        that.view.list.show(new ListView({ collection: anneescolaires }));
      }
    });
    anneescolaireChannel.on('delete:anneescolaire', function(id) {
      var anneescolaire = new Anneescolaire({ ANNEESCOL: id });
      anneescolaire.destroy();
    });
  },

  showAnneescolaire: function(id) {
    var that = this;
    var anneescolaire = new Anneescolaire({ ANNEESCOL: id });
    anneescolaire.fetch({
      success: function(anneescolaire) {
        that.view = new ShowView({ model: anneescolaire });
        that.container.show(that.view);
      }
    });
  },

  editAnneescolaire: function(id) {
    var that = this;
    var anneescolaire = new Anneescolaire({ ANNEESCOL: id });
    anneescolaire.fetch({
      success: function(anneescolaire) {
        that.view = new EditView({ model: anneescolaire });
        that.view.on('edit:anneescolaire', function(data) {
          that.view.model.save( data, {
            success: function() {
              that.showAnneescolaires();
              that.router.navigate('/anneescolaires');
            }
          })
        });
        that.container.show(that.view);
      }
    });
  },

  createAnneescolaire: function() {
    var that = this;
    var anneescolaire = new Anneescolaire();
    this.view = new NewView({ model: anneescolaire });
    this.view.on('save:anneescolaire', function(data) {
      anneescolaire.save( data, {
        success: function(anneescolaire) {
          that.showAnneescolaires();
          that.router.navigate('/anneescolaires');
        }
      });
    });
    this.container.show(this.view);
  }
});