var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'anneescolaires': 'showAnneescolaires',
		'anneescolaire/:id': 'showAnneescolaire',
		'anneescolaire/edit/:id': 'editAnneescolaire',
		'anneescolaires/new': 'createAnneescolaire'
	}
});