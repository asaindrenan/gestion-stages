var Marionette = require('marionette');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/new'),
	className: 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main',

	events: {
		'click .btn': 'save'
	},

	serializeData: function() {
		var model = this.model.toJSON();
		var selects = this.options.selects;
		model.select_anneescol = new Backbone.Collection(selects.get('anneescol')).toJSON();
		model.select_etudiants = new Backbone.Collection(selects.get('etudiants')).toJSON();
		model.select_maitreStages = new Backbone.Collection(selects.get('maitreStages')).toJSON();
		model.select_professeurs = new Backbone.Collection(selects.get('professeurs')).toJSON();
		model.select_organisations = new Backbone.Collection(selects.get('organisations')).toJSON();
		

		console.log(model);
		return model;
	},

	save: function(e) {
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this);
		this.trigger('save:stage', data);
	}
});