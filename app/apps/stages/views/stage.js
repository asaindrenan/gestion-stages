var Marionette = require('marionette');
var radio = require('../../../radio');

var stageChannel = radio.channel('stage');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/stage'),
	tagName: 'tr',

	ui: {
		'delete': '.delete'
	},

	events: {
		'click .delete': 'delete'
	},

	serializeData: function() {
		var model = this.model.toJSON();
		switch(model.IDROLE) {
			case '1':
				model.ROLE = 'Administrateur';
				break;
			case '2':
				model.ROLE = 'Secrétaire';
				break;
			case '3':
				model.ROLE = 'Professeur';
				break;
			case '4':
				model.ROLE = 'Étudiant';
				break;
			case '5':
				model.ROLE = 'Maître de stage';
				break;
		}
		return model;
	},

	delete: function() {
		if(confirm('Êtes-vous sure de vouloir supprimer cette entreprise?')) {
			stageChannel.trigger('delete:stage', this.ui.delete.data('id'));
		}
	}
});