var Marionette = require('marionette');
var radio      = require('../../../radio');

var stageChannel = radio.channel('stage');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/search'),

	ui: {
		'classe': '.classe',
		'annee': '.annee',
		'search': '.search'
	},

	events: {
		'change @ui.classe': 'filterByClasse',
		'change @ui.annee': 'filterByAnnee',
		'keyup @ui.search': 'filterByName'
	},

	serializeData: function() {
		var model = this.model.toJSON();
		return model; 
	},

	filterByName: function(el) {
		var q = $(el.currentTarget).val();
		q.toLowerCase();
		stageChannel.command('filterByName', q);
	},

	filterByClasse: function(el) {
		var q = $(el.currentTarget).val();
		stageChannel.command('filterByClasse', q);
	},

	filterByAnnee: function(el) {
		var q = $(el.currentTarget).val();
		stageChannel.command('filterByAnnee', q);
	}
});