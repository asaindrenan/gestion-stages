var Marionette = require('marionette');
var radio      = require('../../../radio');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/edit'),
	className: 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main',
	
	events: {
		'click .btn': 'edit'
	},

	serializeData: function() {
		var model = this.model.toJSON();
		var selects = this.options.selects;
		model.select_anneescol = new Backbone.Collection(selects.get('anneescol')).toJSON();
		model.select_etudiants = new Backbone.Collection(selects.get('etudiants')).toJSON();
		model.select_maitreStages = new Backbone.Collection(selects.get('maitreStages')).toJSON();
		model.select_professeurs = new Backbone.Collection(selects.get('professeurs')).toJSON();
		model.select_organisations = new Backbone.Collection(selects.get('organisations')).toJSON();
		return model;
	},

	onShow: function(){
		this.$('#listeEtudiants option[value='+this.model.get('IDETUDIANT')+']').attr("selected", "selected");
		this.$('#listeAnneescol option[value='+this.model.get('ANNEESCOL')+']').attr("selected", "selected");
		this.$('#listeProfesseurs option[value='+this.model.get('IDPROFESSEUR')+']').attr("selected", "selected");
		this.$('#listeMaitresStages option[value='+this.model.get('IDMAITRESTAGE')+']').attr("selected", "selected");
		this.$('#listeOrganisations option[value='+this.model.get('IDORGANISATION')+']').attr("selected", "selected");
		this.$('input[value="'+this.model.get('PARTICIPATIONCCF')+'"]').attr("checked", "checked");
	},

	edit: function(e) {
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this);
		this.trigger('edit:stage', data);
	}
});