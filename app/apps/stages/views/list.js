var Marionette = require('marionette');
var radio = require('../../../radio');

var StageView = require('./stage');
var EmptyView = require('./empty');

var stageChannel = radio.channel('stage');

module.exports = Marionette.CompositeView.extend({
	template: require('../templates/list'),
	childView: StageView,
	emptyView: EmptyView,
	childViewContainer: 'tbody',

	onShow: function() {
		var that = this;
		stageChannel.on('delete:stage', function(id) {
			that.collection.remove({ id: id });
		});
	}
});