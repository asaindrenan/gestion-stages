var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'stages': 'showStages',
		'stage/:id': 'showStage',
		'stage/edit/:id': 'editStage',
		'stages/new': 'createStage'
	}
});