var Marionette = require('marionette');
var Backbone = require('backbone');
var radio = require('../../radio');

var Router = require('./router');

var ListView = require('./views/list');
var SearchView = require('./views/search');
var ListLayoutView = require('./views/listLayout');
var ShowView = require('./views/show');
var EditView = require('./views/edit');
var NewView  = require('./views/new');

var Stage  = require('./models/stage');
var Stages = require('./models/stages');

var stageChannel = radio.channel('stage');

module.exports = Marionette.Object.extend({
	initialize: function(options) {
		this.container = options.container;
		this.router = new Router({ controller: this });

	},

  showStages: function() {
    var that = this;

    var Model = Backbone.Model.extend({
      urlRoot: api + 'selects'
    });
    var selects = new Model();
    selects.fetch({
      success: function(data) {
        that.options.selects = data;
      }
    });
    var stages = new Stages();
    stages.fetch({
      success: function(stages){
        var classes = stages.toJSON();
        classes = _.uniq(_.pluck(classes, 'NUMCLASSE'));
        var annees = stages.toJSON();
        annees = _.uniq(_.pluck(annees, 'ANNEESCOL'));
        var Model = Backbone.Model.extend({
          annees: [],
          classes: []
        });
        var model = new Model({
          annees: annees,
          classes: classes
        });
        that.view = new ListLayoutView();
        that.container.show(that.view);
        that.view.search.show(new SearchView({ model: model }));
        that.view.list.show(new ListView({ collection: stages }));
        stageChannel.comply('filterByName', function(q) {
          if(q){
            that.view.list.currentView.filter = function(child, index, collection) {
              var a = child.get('ETUDIANT').toLowerCase();
              var r = (a.indexOf(q) > -1) ? true : false;
              return r;
            }
            that.view.list.currentView.render();
          } else {
            that.view.list.currentView.filter = null;
            that.view.list.currentView.render();
          }
        });
        stageChannel.comply('filterByClasse', function(q) {
          that.view.list.currentView.filter = function(child, index, collection) {
            var r = child.get('NUMCLASSE') === q ? true : false;
            return r;
          }
          that.view.list.currentView.render();
        });
        stageChannel.comply('filterByAnnee', function(q) {
          that.view.list.currentView.filter = function(child, index, collection) {
            var r = child.get('ANNEESCOL') === q ? true : false;
            return r;
          }
          that.view.list.currentView.render();
        });
      }
    });
    stageChannel.on('delete:stage', function(id) {
      var stage = new Stage({ NUM_STAGE: id });
      stage.destroy();
    });
  },

  showStage: function(id) {
    var that = this;
    var stage = new Stage({ NUM_STAGE: id });
    stage.fetch({
      success: function(stage) {
        that.view = new ShowView({ model: stage });
        that.container.show(that.view);
      }
    });
  },

  editStage: function(id) {
    var that = this;
    var stage = new Stage({ NUM_STAGE: id });
    stage.fetch({
      success: function(stage) {
        that.view = new EditView({ model: stage, selects: that.options.selects });
        that.view.on('edit:stage', function(data) {
          that.view.model.save( data, {
            success: function() {
              that.showUsers();
              that.router.navigate('/stages');
            }
          })
        });
        that.container.show(that.view);
      }
    });
  },

  createStage: function() {
    var that = this;
    var stage = new Stage();
    this.view = new NewView({ model: stage, selects: that.options.selects });
    this.view.on('save:stage', function(data) {
      console.log(data);
      stage.save( data, {
        success: function(stage) {
          that.showStages();
          that.router.navigate('/stages');
        }
      });
    });
    this.container.show(this.view);
  }
});