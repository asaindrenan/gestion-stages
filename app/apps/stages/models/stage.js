var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
	urlRoot: api + 'stages',

	idAttribute: 'NUM_STAGE',

	defaults: {
		ANNEESCOL: '',
		IDETUDIANT: '',
		IDPROFESSEUR: '',
		IDORGANISATION: '',
		IDMAITRESTAGE: '',
		DATEDEBUT: '',
		DATEFIN: '',
		DATEVISITESTAGE: '',
		VILLE: '',
		DIVERS: '',
		BILANTRAVAUX: '',
		RESSOURCESOUTILS: '',
		COMMENTAIRES: '',
		PARTICIPATIONCCF:''
	}
});