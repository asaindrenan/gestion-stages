var Marionette = require('marionette');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/dashboard'),
	tagName: 'div',
	className: 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main'
});