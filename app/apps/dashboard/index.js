var Marionette = require('marionette');
var Backbone = require('backbone');

var DashboardView = require('./views/dashboard');

var Router = require('./router');

module.exports = Marionette.Object.extend({
  initialize: function(options) {
    this.container = options.container;
    this.router = new Router({ controller: this });
  },

  showDashboard: function() {
    this.view = new DashboardView();
    this.container.show(this.view);
  }
});