var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'home': 'showDashboard'
	}
});