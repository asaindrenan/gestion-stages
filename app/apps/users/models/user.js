var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
	urlRoot: api + 'personnes',

	idAttribute: 'IDPERSONNE',

	defaults: {
		ID_SPECIALITE: '',
		IDROLE: '',
		CIVILITE: '',
		NOM: '',
		PRENOM: '',
		NUM_TEL: '',
		ADRESSE_MAIL: '',
		NUM_TEL_MOBILE: '',
		ETUDES: '',
		FORMATION: '',
		LOGINUTILISATEUR: '',
		MDPUTILISATEUR: ''
	}
});