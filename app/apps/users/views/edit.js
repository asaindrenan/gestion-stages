var Marionette = require('marionette');
var radio      = require('../../../radio');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/edit'),
	className: 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main',
	
	ui: {
		'roles': 'select[name="IDROLE"]',
		'etudiant': '.champs-etudiant'
	},

	events: {
		'click .btn': 'edit',
		'change @ui.roles': 'displayFields'
	},

	serializeData: function() {
		var model = this.model.toJSON();
		model.annees = this.options.selects.attributes.anneescol;
		model.classes = this.options.selects.attributes.classes;
		return model;
	},

	onShow: function() {
		this.$('#listeRoles option[value='+this.model.get('IDROLE')+']').attr("selected", "selected");
		this.$('#listeCivilites option[value='+this.model.get('CIVILITE')+']').attr("selected", "selected");
		this.$('#listeAnneescol option[value='+this.model.get('ANNEESCOL')+']').attr("selected", "selected");
		this.$('#listeClasses option[value='+this.model.get('NUMCLASSE')+']').attr("selected", "selected");

	},

	displayFields: function(e) {
		var el = $(e.currentTarget);
		if(el.val() == '4') {
			this.ui.etudiant.show();
		} else {
			this.ui.etudiant.hide();
		}
	},

	edit: function(e) {
		var that = this;
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this);
		this.trigger('edit:user', data);
	}
});