var Marionette = require('marionette');
var radio = require('../../../radio');

var UserView = require('./user');
var EmptyView = require('./empty');

var userChannel = radio.channel('user');

module.exports = Marionette.CompositeView.extend({
	template: require('../templates/list'),
	childView: UserView,
	emptyView: EmptyView,
	childViewContainer: 'tbody',

	onShow: function() {
		var that = this;
		userChannel.on('delete:user', function(id) {
			that.collection.remove({ id: id });
		});
	}
});