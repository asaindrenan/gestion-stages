var Marionette = require('marionette');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/new'),
	className: 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main',

	ui: {
		'roles': 'select[name="IDROLE"]',
		'etudiant': '.champs-etudiant'
	},

	events: {
		'click .btn': 'save',
		'change @ui.roles': 'displayFields'
	},

	serializeData: function() {
		var model = this.model.toJSON();
		model.annees = this.options.selects.attributes.anneescol;
		model.classes = this.options.selects.attributes.classes;
		return model;
	},

	displayFields: function(e) {
		var el = $(e.currentTarget);
		if(el.val() == '4') {
			this.ui.etudiant.show();
		} else {
			this.ui.etudiant.hide();
		}
	},

	save: function(e) {
		var that = this;
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this);
		this.trigger('save:user', data);
	}
});