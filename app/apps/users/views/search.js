var Marionette = require('marionette');
var radio      = require('../../../radio');

var userChannel = radio.channel('user');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/search'),

	ui: {
		'search': '.search',
		'role': '.role'
	},

	events: {
		'keyup @ui.search': 'filterByName',
		'change @ui.role': 'filterByRole'
	},

	filterByName: function(el) {
		var q = $(el.currentTarget).val();
		q.toLowerCase();
		userChannel.command('filterByName', q);
	},

	filterByRole: function(el) {
		var q = $(el.currentTarget).val();
		userChannel.command('filterByRole', q);
	}
});