var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'users': 'showUsers',
		'user/:id': 'showUser',
		'user/edit/:id': 'editUser',
		'users/new': 'createUser'
	}
});