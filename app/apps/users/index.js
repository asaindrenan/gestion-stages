var Marionette = require('marionette');
var Backbone = require('backbone');
var radio = require('../../radio');

var Router = require('./router');

var ListView = require('./views/list');
var SearchView = require('./views/search');
var ListLayoutView = require('./views/listLayout');
var ShowView = require('./views/show');
var EditView = require('./views/edit');
var NewView  = require('./views/new');

var User  = require('./models/user');
var Users = require('./models/users');

var userChannel = radio.channel('user');

module.exports = Marionette.Object.extend({
	initialize: function(options) {
		this.container = options.container;
		this.router = new Router({ controller: this });
	},

  showUsers: function() {
    var that = this;

    var Model = Backbone.Model.extend({
      urlRoot: api + 'selects'
    });
    var selects = new Model();
    selects.fetch({
      success: function(data) {
        that.options.selects = data;
      }
    });

    var users = new Users();
    users.fetch({
      success: function(users){
        that.view = new ListLayoutView();
        that.container.show(that.view);
        that.view.search.show(new SearchView());
        that.view.list.show(new ListView({ collection: users }));
        userChannel.comply('filterByName', function(q) {
          if(q){
            that.view.list.currentView.filter = function(child, index, collection) {
              var a = child.get('PRENOM').toLowerCase();
              var b = child.get('NOM').toLowerCase();
              var r = ((a.indexOf(q) > -1) || (b.indexOf(q) > -1)) ? true : false;
              return r;
            }
            that.view.list.currentView.render();
          } else {
            that.view.list.currentView.filter = null;
            that.view.list.currentView.render();
          }
        });
        userChannel.comply('filterByRole', function(q) {
          that.view.list.currentView.filter = function(child, index, collection) {
            var r = child.get('IDROLE') === q ? true : false;
            return r;
          }
          that.view.list.currentView.render();
        });
      }
    });
    userChannel.on('delete:user', function(id) {
      var user = new User({ IDPERSONNE: id });
      user.destroy();
    });
  },

  showUser: function(id) {
    var that = this;
    var user = new User({ IDPERSONNE: id });
    user.fetch({
      success: function(user) {
        that.view = new ShowView({ model: user });
        that.container.show(that.view);
      }
    });
  },

  editUser: function(id) {
    var that = this;
    var user = new User({ IDPERSONNE: id });
    user.fetch({
      success: function(user) {
        that.view = new EditView({ model: user, selects: that.options.selects });
        that.view.on('edit:user', function(data) {
          that.view.model.save( data, {
            success: function() {
              that.showUsers();
              that.router.navigate('/users');
            }
          })
        });
        that.container.show(that.view);
      }
    });
  },

  createUser: function() {
    var that = this;
    var user = new User();
    this.view = new NewView({ model: user, selects: that.options.selects });
    this.view.on('save:user', function(data) {
      user.save( data, {
        success: function(user) {
          that.showUsers();
          that.router.navigate('/users');
        }
      });
    });
    this.container.show(this.view);
  }
});