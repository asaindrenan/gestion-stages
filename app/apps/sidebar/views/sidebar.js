var Marionette = require('marionette');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/sidebar'),
	tagName: 'div',
	className: 'col-sm-3 col-md-2 sidebar'
});