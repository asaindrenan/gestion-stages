var Marionette = require('marionette');
var Backbone = require('backbone');

var SidebarView = require('./views/sidebar');

module.exports = Marionette.Object.extend({
  initialize: function(options) {
    this.container = options.container;
  },

  showSidebar: function() {
    this.view = new SidebarView();
    this.container.show(this.view);
  }
});