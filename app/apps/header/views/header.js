var Marionette = require('marionette');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/header'),
	tagName: 'nav',
	className: 'navbar navbar-inverse navbar-fixed-top'
});