var Marionette = require('marionette');
var Backbone = require('backbone');

var HeaderView = require('./views/header');

module.exports = Marionette.Object.extend({
  initialize: function(options) {
    this.container = options.container;
  },

  showHeader: function() {
    this.view = new HeaderView();
    this.container.show(this.view);
  }
});