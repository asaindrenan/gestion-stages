var Marionette = require('marionette');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/new'),
	className: 'col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main',

	events: {
		'click .btn': 'save'
	},

	serializeData: function() {
		var model = this.model.toJSON();
		model.specialites = this.options.specialites;
		console.log(model);
		return model;
	},

	save: function(e) {
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this);
		this.trigger('save:classe', data);
	}
});