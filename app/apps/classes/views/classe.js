var Marionette = require('marionette');
var radio = require('../../../radio');

var classeChannel = radio.channel('classe');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/classe'),
	tagName: 'tr',

	ui: {
		'delete': '.delete'
	},

	events: {
		'click .delete': 'delete'
	},

	delete: function() {
		if(confirm('Êtes-vous sure de vouloir supprimer cette classe?')) {
			classeChannel.trigger('delete:classe', this.ui.delete.data('id'));
		}
	}
});