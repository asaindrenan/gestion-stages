var Marionette = require('marionette');
var radio      = require('../../../radio');

var classeChannel = radio.channel('classe');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/search'),

	ui: {
		'search': '.search'
	},

	events: {
		'keyup @ui.search': 'filterByName',
	},

	filterByName: function(el) {
		var q = $(el.currentTarget).val();
		q.toLowerCase();
		classeChannel.command('filterByName', q);
	}
});