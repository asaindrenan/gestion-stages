var Marionette = require('marionette');
var radio = require('../../../radio');

var ClasseView = require('./classe');
var EmptyView = require('./empty');

var classeChannel = radio.channel('classe');

module.exports = Marionette.CompositeView.extend({
	template: require('../templates/list'),
	childView: ClasseView,
	emptyView: EmptyView,
	childViewContainer: 'tbody',

	onShow: function() {
		var that = this;
		classeChannel.on('delete:classe', function(id) {
			that.collection.remove({ id: id });
		});
	}
});