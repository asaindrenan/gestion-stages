var Marionette = require('marionette');
var Backbone = require('backbone');
var radio = require('../../radio');

var Router = require('./router');

var ListView = require('./views/list');
var SearchView = require('./views/search');
var ListLayoutView = require('./views/listLayout');
var ShowView = require('./views/show');
var EditView = require('./views/edit');
var NewView  = require('./views/new');

var Classe  = require('./models/classe');
var Classes = require('./models/classes');

var classeChannel = radio.channel('classe');

module.exports = Marionette.Object.extend({
	initialize: function(options) {
		this.container = options.container;
		this.router = new Router({ controller: this });
	},

  showClasses: function() {
    var that = this;

    var Model = Backbone.Model.extend({
      urlRoot: 'http://127.0.0.1/root/api/selects'
    });
    var selects = new Model();
    selects.fetch({
      success: function(selects) {
        that.options.selects = selects;
      }
    });

    var classes = new Classes();
    classes.fetch({
      success: function(classes){
        that.view = new ListLayoutView();
        that.container.show(that.view);
        that.view.search.show(new SearchView());
        that.view.list.show(new ListView({ collection: classes }));
        classeChannel.comply('filterByNameAndVille', function(q) {
          if(q){
            that.view.list.currentView.filter = function(child, index, collection) {
              var a = child.get('NUMCLASSE').toLowerCase();
              var b = child.get('NOMCLASSE').toLowerCase();
              var r = ((a.indexOf(q) > -1) || (b.indexOf(q) > -1)) ? true : false;
              return r;
            }
            that.view.list.currentView.render();
          } else {
            that.view.list.currentView.filter = null;
            that.view.list.currentView.render();
          }
        });
      }
    });
    classeChannel.on('delete:classe', function(id) {
      var classe = new Classe({ NUMCLASSE: id });
      classe.destroy();
    });
  },

  showClasse: function(id) {
    var that = this;
    var classe = new Classe({ NUMCLASSE: id });
    classe.fetch({
      success: function(classe) {
        that.view = new ShowView({ model: classe });
        that.container.show(that.view);
      }
    });
  },

  editClasse: function(id) {
    var that = this;
    var classe = new Classe({ NUMCLASSE: id });
    classe.fetch({
      success: function(classe) {
        that.view = new EditView({ model: classe, specialites: that.options.selects.get('specialites') });
        that.view.on('edit:classe', function(data) {
          that.view.model.save( data, {
            success: function() {
              that.showClasses();
              that.router.navigate('/classes');
            }
          })
        });
        that.container.show(that.view);
      }
    });
  },

  createClasse: function() {
    var that = this;
    var classe = new Classe();
    this.view = new NewView({ model: classe, specialites: that.options.selects.get('specialites') });
    this.view.on('save:classe', function(data) {
      classe.save( data, {
        success: function(classe) {
          that.showClasses();
          that.router.navigate('/classes');
        }
      });
    });
    this.container.show(this.view);
  }
});