var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'classes': 'showClasses',
		'classe/:id': 'showClasse',
		'classe/edit/:id': 'editClasse',
		'classes/new': 'createClasse'
	}
});