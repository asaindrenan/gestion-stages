var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
	urlRoot: api + 'classes',

	idAttribute: 'NUMCLASSE',

	defaults: {
		IDSPECIALITE: '',
		NUMFILIERE: '',
		NOMCLASSE: ''
	}
});