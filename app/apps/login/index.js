var Marionette = require('marionette');
var Backbone = require('backbone');
var radio = require('../../radio');

var LoginView = require('./views/login');

var Router = require('./router');

var loginChannel = radio.channel('login');

module.exports = Marionette.Object.extend({
  initialize: function(options) {
    this.container = options.container;
    this.router = new Router({ controller: this });
    this.showLogin();
  },

  showLogin: function() {
    $('#gestage-login').show();
    this.view = new LoginView();
    this.view.on('login:user', function(data) {
    	loginChannel.command('set:user', data);
    	var Model = Backbone.Model.extend({
    		urlRoot: api + 'login'
    	});
    	var user = new Model();
    	user.save(data, {
    		success: function(user) {
    			loginChannel.command('auth:success', user.attributes);
    		}
    	});
    });
    this.container.show(this.view);
  },

  logout: function() {
    window.location.reload();
  }
});