var Marionette = require('marionette');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/login'),

	ui: {
		'login': 'input[name="login"]',
		'password': 'input[name="password"]',
		'send': '.btn'
	},

	events: {
		'click @ui.send': 'connect'
	},

	connect: function(e) {
		var that = this;
		e.preventDefault();
		var data = Backbone.Syphon.serialize(this);
		this.trigger('login:user', data);
	}

});