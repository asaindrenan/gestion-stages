var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'logout': 'logout'
	}
});