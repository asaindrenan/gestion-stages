var Marionette = require('marionette');
var Backbone = require('backbone');
var radio = require('../../radio');

var Router = require('./router');

var ListView = require('./views/list');
var SearchView = require('./views/search');
var ListLayoutView = require('./views/listLayout');
var ShowView = require('./views/show');
var EditView = require('./views/edit');
var NewView  = require('./views/new');

var Entreprise  = require('./models/entreprise');
var Entreprises = require('./models/entreprises');

var entrepriseChannel = radio.channel('entreprise');

module.exports = Marionette.Object.extend({
	initialize: function(options) {
		this.container = options.container;
		this.router = new Router({ controller: this });
	},

  showEntreprises: function() {
    var that = this;
    var entreprises = new Entreprises();
    entreprises.fetch({
      success: function(entreprises){
        that.view = new ListLayoutView();
        that.container.show(that.view);
        that.view.search.show(new SearchView());
        that.view.list.show(new ListView({ collection: entreprises }));
        entrepriseChannel.comply('filterByNameAndVille', function(q) {
          if(q){
            that.view.list.currentView.filter = function(child, index, collection) {
              var a = child.get('NOM_ORGANISATION').toLowerCase();
              var b = child.get('VILLE_ORGANISATION').toLowerCase();
              var r = ((a.indexOf(q) > -1) || (b.indexOf(q) > -1)) ? true : false;
              return r;
            }
            that.view.list.currentView.render();
          } else {
            that.view.list.currentView.filter = null;
            that.view.list.currentView.render();
          }
        });
      }
    });
    entrepriseChannel.on('delete:entreprise', function(id) {
      var entreprise = new Entreprise({ IDORGANISATION: id });
      entreprise.destroy();
    });
  },

  showEntreprise: function(id) {
    var that = this;
    var entreprise = new Entreprise({ IDORGANISATION: id });
    entreprise.fetch({
      success: function(entreprise) {
        that.view = new ShowView({ model: entreprise });
        that.container.show(that.view);
      }
    });
  },

  editEntreprise: function(id) {
    var that = this;
    var entreprise = new Entreprise({ IDORGANISATION: id });
    entreprise.fetch({
      success: function(entreprise) {
        that.view = new EditView({ model: entreprise });
        that.view.on('edit:entreprise', function(data) {
          that.view.model.save( data, {
            success: function() {
              that.showEntreprises();
              that.router.navigate('/entreprises');
            }
          })
        });
        that.container.show(that.view);
      }
    });
  },

  createEntreprise: function() {
    var that = this;
    var entreprise = new Entreprise();
    this.view = new NewView({ model: entreprise });
    this.view.on('save:entreprise', function(data) {
      entreprise.save( data, {
        success: function(entreprise) {
          that.showEntreprises();
          that.router.navigate('/entreprises');
        }
      });
    });
    this.container.show(this.view);
  }
});