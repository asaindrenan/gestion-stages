var Marionette = require('marionette');
var radio = require('../../../radio');

var entrepriseChannel = radio.channel('entreprise');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/entreprise'),
	tagName: 'tr',

	ui: {
		'delete': '.delete'
	},

	events: {
		'click .delete': 'delete'
	},

	delete: function() {
		if(confirm('Êtes-vous sure de vouloir supprimer cette entreprise?')) {
			entrepriseChannel.trigger('delete:entreprise', this.ui.delete.data('id'));
		}
	}
});