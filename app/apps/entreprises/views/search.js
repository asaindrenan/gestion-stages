var Marionette = require('marionette');
var radio      = require('../../../radio');

var entrepriseChannel = radio.channel('entreprise');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/search'),

	ui: {
		'search': '.search'
	},

	events: {
		'keyup @ui.search': 'filterByNameAndVille',
	},

	filterByNameAndVille: function(el) {
		var q = $(el.currentTarget).val();
		q.toLowerCase();
		entrepriseChannel.command('filterByName', q);
	}
});