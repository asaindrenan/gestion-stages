var Marionette = require('marionette');
var radio = require('../../../radio');

var EntrepriseView = require('./entreprise');
var EmptyView = require('./empty');

var entrepriseChannel = radio.channel('entreprise');

module.exports = Marionette.CompositeView.extend({
	template: require('../templates/list'),
	childView: EntrepriseView,
	emptyView: EmptyView,
	childViewContainer: 'tbody',

	onShow: function() {
		var that = this;
		entrepriseChannel.on('delete:entreprise', function(id) {
			that.collection.remove({ id: id });
		});
	}
});