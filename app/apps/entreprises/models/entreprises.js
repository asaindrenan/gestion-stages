var Backbone = require('backbone');

var Entreprise = require('./entreprise');

module.exports = Backbone.Collection.extend({
	url: api + 'organisations',
	model: Entreprise
});