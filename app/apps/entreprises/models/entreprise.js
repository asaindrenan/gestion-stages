var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
	urlRoot: api + 'organisations',

	idAttribute: 'IDORGANISATION',

	defaults: {
		NOM_ORGANISATION: '',
		VILLE_ORGANISATION: '',
		ADRESSE_ORGANISATION: '',
		CP_ORGANISATION: '',
		TEL_ORGANISATION: '',
		FAX_ORGANISATION: '',
		FORMEJURIDIQUE: '',
		ACTIVITE: ''
	}
});