var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'entreprises': 'showEntreprises',
		'entreprise/:id': 'showEntreprise',
		'entreprise/edit/:id': 'editEntreprise',
		'entreprises/new': 'createEntreprise'
	}
});