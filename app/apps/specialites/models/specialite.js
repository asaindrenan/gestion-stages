var Backbone = require('backbone');

module.exports = Backbone.Model.extend({
	urlRoot: api + 'specialites',

	idAttribute: 'IDSPECIALITE',

	defaults: {
		'LIBELLECOURTSPECIALITE': '',
		'LIBELLELONGSPECIALITE': ''
	}
});