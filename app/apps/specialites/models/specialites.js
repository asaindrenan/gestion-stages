var Backbone = require('backbone');

var Specialite = require('./specialite');

module.exports = Backbone.Collection.extend({
	url: api + 'specialites',
	model: Specialite
});