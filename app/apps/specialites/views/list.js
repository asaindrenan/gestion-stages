var Marionette = require('marionette');
var radio = require('../../../radio');

var SpecialiteView = require('./specialite');
var EmptyView = require('./empty');

var specialiteChannel = radio.channel('specialite');

module.exports = Marionette.CompositeView.extend({
	template: require('../templates/list'),
	childView: SpecialiteView,
	emptyView: EmptyView,
	childViewContainer: 'tbody',

	onShow: function() {
		var that = this;
		specialiteChannel.on('delete:specialite', function(id) {
			that.collection.remove({ id: id });
		});
	}
});