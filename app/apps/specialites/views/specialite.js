var Marionette = require('marionette');
var radio = require('../../../radio');

var specialiteChannel = radio.channel('specialite');

module.exports = Marionette.ItemView.extend({
	template: require('../templates/specialite'),
	tagName: 'tr',

	ui: {
		'delete': '.delete'
	},

	events: {
		'click .delete': 'delete'
	},

	delete: function() {
		if(confirm('Êtes-vous sure de vouloir supprimer cette année scolaire?')) {
			specialiteChannel.trigger('delete:specialite', this.ui.delete.data('id'));
		}
	}
});