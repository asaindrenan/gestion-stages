var Marionette = require('marionette');

module.exports = Marionette.AppRouter.extend({
	appRoutes: {
		'specialites': 'showSpecialites',
		'specialite/edit/:id': 'editSpecialite',
		'specialites/new': 'createSpecialite'
	}
});