var Marionette = require('marionette');
var Backbone = require('backbone');
var radio = require('../../radio');

var Router = require('./router');

var ListView = require('./views/list');
var SearchView = require('./views/search');
var ListLayoutView = require('./views/listLayout');
var EditView = require('./views/edit');
var NewView  = require('./views/new');

var Specialite  = require('./models/specialite');
var Specialites = require('./models/specialites');

var specialiteChannel = radio.channel('specialite');

module.exports = Marionette.Object.extend({
	initialize: function(options) {
		this.container = options.container;
		this.router = new Router({ controller: this });
	},

  showSpecialites: function() {
    var that = this;
    var specialites = new Specialites();
    specialites.fetch({
      success: function(specialites){
        that.view = new ListLayoutView();
        that.container.show(that.view);
        that.view.search.show(new SearchView());
        that.view.list.show(new ListView({ collection: specialites }));
      }
    });
    specialiteChannel.on('delete:specialite', function(id) {
      var specialite = new Specialite({ IDSPECIALITE: id });
      specialite.destroy();
    });
  },

  editSpecialite: function(id) {
    var that = this;
    var specialite = new Specialite({ IDSPECIALITE: id });
    specialite.fetch({
      success: function(specialite) {
        that.view = new EditView({ model: specialite });
        that.view.on('edit:specialite', function(data) {
          that.view.model.save( data, {
            success: function() {
              that.showSpecialites();
              that.router.navigate('/specialites');
            }
          })
        });
        that.container.show(that.view);
      }
    });
  },

  createSpecialite: function() {
    var that = this;
    var specialite = new Specialite();
    this.view = new NewView({ model: specialite });
    this.view.on('save:specialite', function(data) {
      specialite.save( data, {
        success: function(specialite) {
          that.showSpecialites();
          that.router.navigate('/specialites');
        }
      });
    });
    this.container.show(this.view);
  }
});